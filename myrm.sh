#!/bin/bash
read fp
osascript << EOF
tell application "Finder"
    posix path of ((delete posix file "${fp}") as unicode text)
end tell
EOF

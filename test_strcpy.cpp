#include<iostream>
#include<string>
#include<regex>
#include<ctime>
using namespace std;
#define flag regex_constants::ECMAScript
int main(int argc,char* argv[]){
    if(argc>=2){
        string str(argv[1]);
        cout<<str<<endl;
        // str.clear();
        str.insert(str.length(),"我是机器人");
        char cstr[10];
        strcpy(cstr,"abc");
        str.insert(str.length(),cstr);
        cout<<str<<endl;
    }
    if(false){
        string str(argv[1]);
        const char* cstr=str.c_str();
        char ls[100];
        strcpy(ls,"ls -l ");
        int count(strlen(ls));
        for(int i(0);i<strlen(cstr);i++)ls[i+count]=cstr[i];
        cout<<ls<<endl;
        FILE *pp = popen(ls, "r"); // build pipe
        if (!pp)
            return 1;

        // collect cmd execute result
        char tmp[1024];
        while (fgets(tmp, sizeof(tmp), pp) != NULL)
            std::cout << tmp << std::endl; // can join each line as string
        pclose(pp);
    }
    char chstr[100];
    strcpy(chstr,"char[]");
    string str("string");
    string str_joint;
    str_joint.append(chstr);
    str_joint.append(str);
    cout<<str_joint<<endl;
    regex regd("-d",flag);	// 去除，mv直接移动目录
	regex regf("-f",flag);	// 保留，与mv -f作用相同
	regex regi("-i",flag);	// 保留，与mv -i作用相似
	regex regP("-P",flag);	// 去除
	regex regR("-R",flag);	// 去除，mv直接移动目录
	regex regr("-r",flag);	// 去除，等价于-R
	regex regv("-v",flag);	// 保留，与mv -v作用相同
	regex regW("-W",flag);	// break，Attempt to undelete the named files
	regex reghyphen("--",flag);
	for(int i(1);i<argc;i++){
		bool bd(regex_search(argv[i],regd));
        bool bP(regex_search(argv[i],regP));
        bool bR(regex_search(argv[i],regR));
        bool br(regex_search(argv[i],regr));
        bool bW(regex_search(argv[i],regW));
        bool bhyphen(regex_search(argv[i],reghyphen));
        cout<<bd<<' '<<bP<<' '<<bR<<' '<<br<<' '<<bW<<' '<<bhyphen<<' '<<endl;
    }

    char cmd[2];
    cmd[2]='E';
    cmd[3]='N';
    cmd[4]='D';
    strcpy(cmd,"mv");   // strcpy(char*,const char*)会拷贝const char*的尾后'\0'
    cout<<strlen(cmd)<<endl;
    cout<<'\0'<<'_'<<endl;
    cout<<cmd[2]<<'_'<<endl;    // '\0'是不可打印字符，cout也不打印任何东西
    cout<<cmd[0]<<cmd[1]<<cmd[2]<<cmd[3]<<cmd[4]<<endl;     // mvND，mv后面有'\0'不可打印字符，替换了'E'

    char regstr[2];
    strcpy(regstr,"-r");
    regex reg(regstr);
    cout<<"regex_search(reg)"<<regex_search("-r -v",reg)<<endl;

    char regstr3[3];
    strcpy(regstr3,"-r");
    regex reg3(regstr3);
    cout<<"regex_search(reg3)"<<regex_search("-r -v",reg3)<<endl;

    time_t now(time(0));
    tm *now_tm(localtime(&now));
    cout<<now<<endl;
    cout<<now_tm->tm_year+1900<<' '<<now_tm->tm_mon+1<<' '<<now_tm->tm_mday<<endl;
    cout<<now_tm->tm_hour<<' '<<now_tm->tm_min<<' '<<now_tm->tm_sec<<endl;

    char cmdll[]="ls -l";
    cout<<system(cmdll)<<endl;

    char a[16];
    size_t i;
    
    i = snprintf(a, 13, "_%012d_", 12345);  // 第 1 种情况
    printf("i = %lu, a = %s\n", i, a);    // 输出：i = 14, a = _00000001234
    
    i = snprintf(a, 9, "_%012d_", 12345);   // 第 2 种情况
    printf("i = %lu, a = %s\n", i, a);    // 输出：i = 14, a = _0000000

    i = snprintf(a, 16, "_%012d_", 12345);  // 第 1 种情况
    printf("i = %lu, a = %s\n", i, a);    // 输出：i = 14, a = _000000012345_
    return 0;
}
#include <iostream>
#include <regex>
#include <vector>
// #include <fstream>
#include <string>
#include <ctime>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <memory>
using namespace std;
long long getCurrentTime()  
{  
   struct timeval tv;  
   gettimeofday(&tv,NULL);  
   return tv.tv_sec * 1000 + tv.tv_usec / 1000;  
} 

// #include <sys/select.h>
// static void sleep_ms(unsigned int secs)
// {
//     struct timeval tval;
//     tval.tv_sec=secs/1000;
//     tval.tv_usec=(secs*1000)%1000000;
//     select(0,NULL,NULL,NULL,&tval);
// }
#define BUF_SIZE 1<<10
#define flag regex_constants::ECMAScript
class MyRegex{
	public:
	regex rgx;
	template<typename ... Type>
	MyRegex(Type&... args):rgx(args...){};
	~MyRegex(){
		cout<<"destruct rgx("<<endl;
	}
};
bool exec(const char* cmd,bool blog){
	int retcode(system(cmd));
	if(retcode==0) return true;
	else{
		// cout<<cmd<<" return errcode: "<<retcode<<endl;
		return false;
	}
	if(false){
		/*
		FILE * p_file = NULL;  
		p_file = popen(cmd, "r");
		if (!p_file) {  
			fprintf(stderr, "sh error, check rm source code! ! !");  
			return false;
		}
		regex reg("No such file or directory",flag);
		char buf[BUF_SIZE];
		bool bReturn(false);
		char* res=fgets(buf, BUF_SIZE, p_file);
		// mac上不论popen()执行成功与否，此处调用fgets()会引发“[1]    21015 segmentation fault”错误
		cout<<res<<endl;
		while (res != NULL) {
			// https://pubs.opengroup.org/onlinepubs/9699919799/functions/popen.html
			bReturn=true;
			if(blog) 
				cout<<buf<<endl;
			if(regex_search(buf,reg)){
				return false;
			}
			res=fgets(buf, BUF_SIZE, p_file);
		}
		if(! bReturn) return false;
		pclose(p_file);
		return true;
		*/
	}
	
};
bool replace_str(string &input_str,string rgx_str,string replace_str){
	try{
		string str_replace_space = (std::string)"";
		shared_ptr<regex> prgx_space(std::make_shared<regex>(rgx_str,flag));
		regex_replace(std::back_inserter(str_replace_space),input_str.begin(),input_str.end(),*prgx_space.get(),replace_str);
		input_str = str_replace_space;
		return true;
	}catch(std::exception &e){
		cout<<e.what()<<endl;
		return false;
	}
}
#define DEBUG false
int exec(int argc,char *argv[]){
	if(DEBUG){
		// argc = 2;
		// strcpy(argv[1],"haha\ heihei.docx\ ");
		// cout<<argv[1]<<"_"<<endl;
	}
    if(argc<=1){
        cout<<"please input at least one file!"<<endl;
		return 1;
    }
	int iCountSuc(0),iCountFail(0);
	string cmdsls[argc],cmdsmv[argc],errs[argc],sucs[argc];
	vector<string> argv_nopath(argc,""),argv_path(argc,""),argv_nopath_nosuffix(argc,""),argv_suffix(argc,"");
	char mv[BUF_SIZE],to[BUF_SIZE],desti[BUF_SIZE],fail[BUF_SIZE],success[BUF_SIZE];
	strcpy(mv,"mv ");
	strcpy(to," to ");
	strcpy(desti," /Users/haypinfong/.Trash");
	strcpy(fail," FAILED! ! !");
	strcpy(success," SUCCESS");
	// char chard[100],charf[100],chari[100],charP[100];
	// char charR[100],charr[100],charv[100],charW[100];
	// strcpy(chard,"[[:space:]]+-[fiPRrvW]*d");		// strcpy(char*,const char*)不会拷贝const char*的尾后'\0'
	// strcpy(charf,"[[:space:]]+-[diPRrvW]*f");
	// strcpy(chari,"[[:space:]]+-[dfPRrvW]*i");
	// strcpy(charP,"[[:space:]]+-[dfiRrvW]*P");
	// strcpy(charR,"[[:space:]]+-[dfiPrvW]*R");
	// strcpy(charr,"[[:space:]]+-[dfiPRvW]*r");
	// strcpy(charv,"[[:space:]]+-[dfiPRrW]*v");
	// strcpy(charW,"[[:space:]]+-[dfiPRrv]*W");	// 选项之间的空格已经被find -print0给分隔到argv[]了，argv[i]中已经不存在前缀后缀空格
	// "-fidIRrvWxP"
	// MyRegex regf("-[idIRrvWxP]*f[idIRrvWxP]*",flag);
	shared_ptr<regex> prgxf(std::make_shared<regex>("-[idIRrvWxP]*f[idIRrvWxP]*",flag));	// 保留，与mv -f作用相同
	shared_ptr<regex> prgxi(std::make_shared<regex>("-[fdIRrvWxP]*i[fdIRrvWxP]*",flag));	// 保留，与mv -i作用相似
	shared_ptr<regex> prgxd(std::make_shared<regex>("-[fiIRrvWxP]*d[fiIRrvWxP]*",flag));	// 去除，mv直接移动目录
	shared_ptr<regex> prgxI(std::make_shared<regex>("-[fidRrvWxP]*I[fidRrvWxP]*",flag));	// 保留，与mv -i作用相似
	shared_ptr<regex> prgxR(std::make_shared<regex>("-[fidIrvWxP]*R[fidIrvWxP]*",flag));	// 去除，mv直接移动目录
	shared_ptr<regex> prgxr(std::make_shared<regex>("-[fidIRvWxP]*r[fidIRvWxP]*",flag));	// 去除，等价于-R
	shared_ptr<regex> prgxv(std::make_shared<regex>("-[fidIRrWxP]*v[fidIRrWxP]*",flag));	// 保留，与mv -v作用相同
	shared_ptr<regex> prgxW(std::make_shared<regex>("-[fidIRrvxP]*W[fidIRrvxP]*",flag));	// break，Attempt to undelete the named files？
	shared_ptr<regex> prgxx(std::make_shared<regex>("-[fidIRrvWP]*x[fidIRrvWP]*",flag));	// When removing a hierarchy, do not cross mount points？
	shared_ptr<regex> prgxP(std::make_shared<regex>("-[fidIRrvWx]*P[fidIRrvWx]*",flag));	// 去除，OS X的rm没有-P选项
	shared_ptr<regex> prgxhyphen(std::make_shared<regex>("--",flag));
	shared_ptr<regex> prgx_sepa(std::make_shared<regex>("/",flag));
	shared_ptr<regex> prgx_dot(std::make_shared<regex>("\\.",flag));
	bool bhyphen(false);
	string mv_joint;
	mv_joint.append("mv ");
	for(int i(1);i<argc;i++){
		// cout<<"argv["<<i<<"]:_"<<argv[i]<<"_"<<endl;
		if(!bhyphen)	bhyphen=regex_match(argv[i],*prgxhyphen.get());	// 如果前面已经有--了就保持有
		if(!bhyphen){	// --前的都是选项，--后的不是选项
			bool bd(regex_match(argv[i],*prgxd.get()));
			bool bP(regex_match(argv[i],*prgxP.get()));
			bool bR(regex_match(argv[i],*prgxR.get()));
			bool br(regex_match(argv[i],*prgxr.get()));
			bool bW(regex_match(argv[i],*prgxW.get()));
			bool bx(regex_match(argv[i],*prgxx.get()));
			if(bd || bP || bR || br) {
				cout<<"continue option "<<argv[i]<<endl;
				continue;
			}
			if(bW) {
				cout<<"-W mean Attempt to undelete the named files, will break"<<endl;
				break;
			}
			if(bx) {
				cout<<"-x mean When removing a hierarchy, do not cross mount points, will break"<<endl;
				break;
			}
			
			bool bf(regex_match(argv[i],*prgxf.get()));	// Unit pass
			bool bi(regex_match(argv[i],*prgxi.get()));
			bool bI(regex_match(argv[i],*prgxI.get()));
			bool bv(regex_match(argv[i],*prgxv.get()));
			if(bf) {
				mv_joint.append("-f ");
				continue;
			}
			if(bi) {
				mv_joint.append("-i ");
				continue;
			}
			if(bI) {
				mv_joint.append("-i ");
				continue;
			}
			if(bv) {
				mv_joint.append("-v ");
				continue;
			}
		}
		// cout<<"argv["<<i<<"]:_"<<argv[i]<<"_"<<endl;
		// 20220226 将argv[i]从最后的/切掉作为ls的的参数
		// 通道入口命令是find -print0，打印总是全路径，如果搜索到"/"则切分到文件名作为ls的输入argv_nopath
		argv_nopath[i] = (std::string)argv[i];	// 用于ls
		smatch res;
		// cout<<"argv["<<i<<"]:_"<<argv[i]<<"_"<<endl;
		while(regex_search(argv_nopath[i],res,*prgx_sepa.get())){
			argv_nopath[i] = res.suffix().str();
		}
		// replace_str(argv_nopath[i],(std::string)"\\ ",(std::string)" ");
		replace_str(argv_nopath[i],(std::string)" ",(std::string)"\\ ");	// 文件名无路径含转义空格含后缀
		replace_str(argv_nopath[i],(std::string)"\\(",(std::string)"\\(");
		replace_str(argv_nopath[i],(std::string)"\\)",(std::string)"\\)");
		// cout<<"argv_nopath[i]: "<<argv_nopath[i]<<endl;
		
		if(regex_search(argv_nopath[i],res,*prgx_dot.get())){
			
			size_t suffix_idx( argv_nopath[i].find_last_of("\\.") );
			// cout<<argv_nopath[i].substr(0,suffix_idx)<<endl;

			// cout<<res.prefix().str()<<endl;
			argv_nopath_nosuffix[i] = res.prefix().str();	// 用于~/.Trash/xxx20230108.docx
			
			// replace_str(argv_nopath_nosuffix[i],(std::string)"\\ ",(std::string)" ");
			replace_str(argv_nopath_nosuffix[i],(std::string)" ",(std::string)"_");	// 文件名无路径无后缀含转义空格
			replace_str(argv_nopath_nosuffix[i],(std::string)"\\ ",(std::string)"_");
			replace_str(argv_nopath_nosuffix[i],(std::string)"\\(",(std::string)"\\(");
			replace_str(argv_nopath_nosuffix[i],(std::string)"\\)",(std::string)"\\)");
			// replace_str(argv_nopath_nosuffix[i],(std::string)"\\",(std::string)"_");
			// replace_str(argv_nopath_nosuffix[i],(std::string)"\ ",(std::string)"");
			// cout<<argv_nopath[i].substr(suffix_idx)<<endl;
			// cout<<res.suffix().str()<<endl;
			argv_suffix[i] = res.suffix().str();
			argv_suffix[i].insert(0,1,'.');
			while(argv_suffix[i].substr(argv_suffix[i].size()-1)==(std::string)" " ||
				argv_suffix[i].substr(argv_suffix[i].size()-1)==(std::string)"\\")
				argv_suffix[i] = argv_suffix[i].substr(0,argv_suffix[i].size()-1);
			replace_str(argv_suffix[i],(std::string)" ",(std::string)"\\ ");	// 文件名后缀脱去尾空格后含转义空格
			// cout<<argv_suffix[i]<<endl;
			// find -print0 | xargs -0 -I {} ls {}，xargs中调用的命令能正确处理空格，但此处system()调用mv argv_nopath[i]并不能自主处理空格
		}else{
			argv_nopath_nosuffix[i] = argv_nopath[i];
			argv_suffix[i] = "";
		}
		argv_path[i] = (std::string)argv[i];	// 用于mv
		// replace_str(argv_path[i],(std::string)"\\ ",(std::string)" ");
		replace_str(argv_path[i],(std::string)" ",(std::string)"\\ ");	// 文件名全路径含转义空格
		replace_str(argv_path[i],(std::string)"\\(",(std::string)"\\(");
		replace_str(argv_path[i],(std::string)"\\)",(std::string)"\\)");
		// cout<<"argv_path[i]: "<<argv_path[i]<<endl;
		cmdsls[i-1]="";
		cmdsls[i-1].append("ls ");		// ls desti/argv[i]，如果存在就mv argv[i] desti/argv[i]+08-10-14:23:11
		cmdsls[i-1].append(desti);		// /Users/haypinfong/.Transh
		cmdsls[i-1].append("/");
		cmdsls[i-1].append(argv_nopath[i]);
		
		cmdsmv[i-1]="";
		cmdsmv[i-1].append(mv_joint);
		cmdsmv[i-1].append(argv_path[i]);
		cmdsmv[i-1].append(desti);
		try{
			cout<<cmdsls[i-1].c_str()<<endl;
			if(exec(cmdsls[i-1].c_str(),false)){
				time_t now(time(0));
				tm *now_tm(localtime(&now));
				char buf[BUF_SIZE];
				strcpy(buf,"/");
				cmdsmv[i-1].append(buf);			// ~/.Trash/
				cmdsmv[i-1].append(argv_nopath_nosuffix[i]);	// ~/.Trash/file_name
				strcpy(buf,"\\ \\ ");		// 空格转义很重要，
				cmdsmv[i-1].append(buf);			// ~/.Trash/file_name__ 
				snprintf(buf,BUF_SIZE,"%d",now_tm->tm_mon+1);	// 奇葩，月份也是0下标
				cmdsmv[i-1].append(buf);
				cmdsmv[i-1].append("-");			// ~/.Trash/file_name  1-
				snprintf(buf,BUF_SIZE,"%d",now_tm->tm_mday);
				cmdsmv[i-1].append(buf);
				cmdsmv[i-1].append("-");			// ~/.Trash/file_name  1-26-
				snprintf(buf,BUF_SIZE,"%d",now_tm->tm_hour);
				cmdsmv[i-1].append(buf);
				cmdsmv[i-1].append("时");			// ~/.Trash/file_name  1-26-14:
				snprintf(buf,BUF_SIZE,"%d",now_tm->tm_min);
				cmdsmv[i-1].append(buf);
				cmdsmv[i-1].append("分");			// ~/.Trash/file_name  1-26-14:29
				snprintf(buf,BUF_SIZE,"%d",now_tm->tm_sec);
				cmdsmv[i-1].append(buf);
				cmdsmv[i-1].append("秒\\ \\ ");			// ~/.Trash/file_name  1-26-14:29:59
				// sprintf(buf,"%d",int(now));
				snprintf(buf,BUF_SIZE,"%lld",getCurrentTime());// 毫秒
				cmdsmv[i-1].append(buf);			// ~/.Trash/file_name  1-26-14时29分59秒 1645867005306

				cmdsmv[i-1].append(argv_suffix[i]);
			}
		}catch(std::exception &e){
			cout<<e.what()<<endl;
		}
		cout<<cmdsmv[i-1]<<"      #...... "<<endl;
		if(exec(cmdsmv[i-1].c_str(),true)){
			string suc;
			suc.append(mv);
			suc.append(argv[i]);	// humanize with no \space 
			suc.append(to);
			suc.append(desti);
			suc.append(success);
			sucs[iCountSuc]=suc;
			iCountSuc +=1;
			continue;
		}

		string err;
		err.append(mv);
		err.append(argv[i]);
		err.append(to);
		err.append(desti);
		err.append(fail);
		errs[iCountFail]=err;
		iCountFail+=1;

		// 太快了，时间几乎一样
		// sleep_ms(100);
	}  
	// cout<<"break 0"<<endl;
	// delete prgxf;
	// cout<<"break 1"<<endl;
	// delete prgxi;
	// delete prgxd;
	// delete prgxI;
	// delete prgxR;
	// delete prgxr;
	// delete prgxv;
	// delete prgxW;
	// delete prgxx;
	// delete prgxP;
	// delete prgxhyphen;
	// delete prgx_sepa;
	// delete prgx_dot;
	cout<<"=====>>> total "<<endl;
	if(iCountSuc>0){
		// for(string& imv:sucs) cout<<imv<<endl;
		for(size_t i(0);i<iCountSuc;i++) cout<<sucs[i]<<endl;
	}
	if(iCountFail>0){
		// for(string& irr:errs)cout<<irr<<endl;	// 发生错误时system(cmd)会打印标准错误的
		for(size_t i(0);i<iCountFail;i++) cout<<errs[i]<<endl;
	}
	string res;
	res.append("total ");
	char str[BUF_SIZE];
	snprintf(str, BUF_SIZE,"%d", iCountSuc);
	res.append(str);
	res.append(" success");
	// if(iCountFail>=0 || iCountSuc<0){
	res.append(", ");
	snprintf(str, BUF_SIZE,"%d", iCountFail);
	res.append(str);
	res.append(fail);
	// }
	cout<<res<<endl;
	cout<<endl;
	return 0;
}
int main(int argc,char* argv[]){
	// 20220225 还是将可能带空格的文件名列表输出到文件，替换空格生成source_files_abspath，source_files_name
	// vector<string> source_files_abspath,source_files_name;
	// ifstream infile(argv);
	// string litem;
	// regex reg_(" ",flag);
	// while(getline(infile,litem)){
	// }
	try{
		exec(argc,argv);
	}catch(std::exception &e){
		cout<<e.what()<<endl;
	}
	return 0;
}
